
node /riaks./ {

  include packagecloud
  include taskjp

  packagecloud::repo { "basho/riak":
    type => 'deb',
  } ->
  package { "riak":
    ensure => installed,
  }

  firewall { '99 allow 22, 8087, 8098 access':
    dport  => [22, 8087, 8098],
    proto  => tcp,
    action => accept,
  }

  Firewall {
    before  => Class['my_fw::post'],
    require => Class['my_fw::pre'],
  }

  class { ['my_fw::pre', 'my_fw::post']: }

  class { 'firewall': }

}

node "riakc" {

  include taskjp
  include erlang
  include nginx

  package { "git-core":
    ensure => installed,
  }
}

