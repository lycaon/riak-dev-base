Riak Development Base Boxes
===========================

# Two VMs

## 1st: Riak Server

### Installs...

- riak (via packagecloud)
- taskjp

## 2nd: Riak Client

### Installs...

- Latest Erlang (via Erlang Solutions)
- Nginx (for web)
- Git